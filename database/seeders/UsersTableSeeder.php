<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();

        $password = Hash::make('secret');

        User::create([
            'name' => 'mumu',
            'email' => 'muhidin0794@gmail.com',
            'password' => $password,
        ]);
        
        $faker = \Faker\Factory::create();

        for ($i=0; $i < 10; $i++) { 
            # code...
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $password,
            ]);
        }
    }
}
