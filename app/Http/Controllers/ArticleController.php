<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $article = Article::all();

        if($article){

            $data = array(
                'message' => 'Get data success!',
                'code' => 200,
                'status' => 'success',
                'data' => $article,
            );

        } else {

            $data = array(
                'message' => 'Get data failed!',
                'code' => 200,
                'status' => 'failed',
                'data' => '',
            );

        }

        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $article = Article::create($request->all());

        if($article){
            $data = array(
                'message' => 'Store data success!',
                'code' => 200,
                'status' => 'success',
                'data' => $article,
            );
        } else {
            $data = array(
                'message' => 'Store data failed!',
                'code' => 200,
                'status' => 'success',
                'data' => '',
            );
        }

        return response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $article =  Article::find($id);

        // if($article){

        //     $data = array(
        //         'message' => 'Get data success!',
        //         'code' => 200,
        //         'status' => 'success',
        //         'data' => $article,
        //     );
        // } else {
        //     $data = array(
        //         'message' => 'Get data failed!',
        //         'code' => 200,
        //         'status' => 'failed',
        //         'data' => '',
        //     );
        // }
        $data = array(
            'message' => 'Get data success!',
            'code' => 200,
            'status' => 'success',
            'data' => $article,
        );

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $article = Article::findOrFail($id);
        $article->update($request->all());

        if($article) {

            $data = array(
                'message' => 'Update data success!',
                'code' => 200,
                'status' => 'success',
                'data' => $article,
            );

        }else {

            $data = array(
                'message' => 'Update data failed!',
                'code' => 200,
                'status' => 'failed',
                'data' => '',
            );

        }


        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $article = Article::findOrFail($id);
        $article->delete();

        //return 204;
        if($article) {
            $data = array(
                'message' => 'Delete data success!',
                'code' => 200,
                'status' => 'success',
                'data' => $article,
            );
        } else {
            $data = array(
                'message' => 'Delete data failed!',
                'code' => 200,
                'status' => 'success',
                'data' => '',
            );
        }

        return response()->json($data, 200);
    }


}
